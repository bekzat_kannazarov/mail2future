﻿using DataAccessLayer;
using System.Linq;
using System.Web.Mvc;
using DomainModels;
using System.Data;
using MVC.Models;
using System;
using System.Collections.Generic;

namespace MVC.Controllers
{
    public class MessageController : Controller
    {
        private MailContext db = new MailContext();
        // GET: Message
        [Authorize]
        public ActionResult Index()
        {
            using (var db = new MailContext())
            {
                var username = User.Identity.Name;
                var user = db.Users.First(u => u.UserName == username);

                var messages = db.Messages
                    .Where(m => m.UserId == user.Id)
                    .ToList()
                    .Select(message => new MessageListItem
                    {
                        MessageID = message.MessageID,
                        Subject = message.Subject,
                        MailTo = message.MailTo,
                        CreationDate = message.CreationDate,
                        Deadline = message.Deadline,
                        MailContent = message.MailContent,
                        MailFrom = user.Email
                    });
                return View(messages);
            }
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( MessageSubmitModel message)
        {
            
            if (!ModelState.IsValid)
            {
                return View("Create");
            }
            else
            {
                var username = User.Identity.Name;
                var user = db.Users.FirstOrDefault(u => u.UserName == username);
                var messages = db.Messages.Where(m => m.UserId == user.Id);
                Message entity = new Message
                {
                    Subject = message.Subject,
                    MailTo = message.MailTo,
                    CreationDate = DateTime.Now,
                    Deadline = message.Deadline,
                    MailContent = message.MailContent,
                    UserId = user.Id
                };
                db.Messages.Add(entity);
                db.SaveChanges();
                return RedirectToAction("index");
            }

        }

        

    }
}