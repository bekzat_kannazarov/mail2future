﻿using System;
using System.Collections.Generic;
using System.Linq;
using DomainModels;

namespace MVC.Models
{
    public class MessageSubmitModel
    {
        public string Subject { get; set; }
        public string MailTo { get; set; }
        public DateTime Deadline { get; set; }
        public string MailContent { get; set; }
    }
}