﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class MessageListItem
    {
        public int MessageID { get; set; }
        public string Subject { get; set; }
        public string MailTo { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime Deadline { get; set; }
        public string MailContent { get; set; }
        public string MailFrom { get; set; }
    }
}