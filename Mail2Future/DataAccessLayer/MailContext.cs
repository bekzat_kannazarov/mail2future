﻿using DomainModels;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace DataAccessLayer
{
    // В профиль пользователя можно добавить дополнительные данные, если указать больше свойств для класса ApplicationUser. Подробности см. на странице https://go.microsoft.com/fwlink/?LinkID=317594.
    public class MailContext : IdentityDbContext<ApplicationUser>
    {
        public MailContext()
            : base("MailConnection")
        {
        }

        public static MailContext Create()
        {
            return new MailContext();
        }
        public DbSet<Message> Messages { get; set; }
    }
}