﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainModels
{
    [Table("Messages")]
    public class Message
    {
        [Key]
        public int MessageID { get; set; }
        public string Subject { get; set; }
        public string MailTo { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime Deadline { get; set; }
        public string MailContent { get; set; }
        public string UserId { get; set; }
    }
}
